#!/usr/bin/node

//
// CHAT
//

"use strict";

//============================================//

var listenport = 31337;

var kafkahost = 'localhost';
var kafkaport = 9092;
var kafkatopic = 'chatroom';
var kafkapartition = 0;

//============================================//

var WebSocketServer = require('websocket').server;
var http = require('http');

// Logging 
function ln(msg) { console.log( (new Date()) + msg ); }

// Setup HTTP listener
var server = http.createServer(function(request,response) { });
server.listen(listenport, function() {});


// Websockets server 
//
var wsServer = new WebSocketServer( { httpServer: server });
wsServer.on('request', function(req) {
	ln("New connection from " + req.origin + "...");
	var conn = req.accept(null,req.origin);
	ln("... accepted. Connecting to kafka...");
	var kafka_native = require('kafka-native');
	var broker = kafkahost + ':' + kafkaport;

	var producer = new kafka_native.Producer({
	    broker: broker
	});

	producer.partition_count(kafkatopic)
	.then(function(npartitions) {
		ln("Connected to kafka as a producer for "+conn.remoteAddress);
	});

	ln("connected.");
	
	// Handle messages from the client
	conn.on('message', function(msg) {
		if (msg.type === 'utf8') { // Only bother with text messages
			var data = msg.utf8Data;
			ln("Got message from client "+conn.remoteAddress+": " + data);
			producer.send(kafkatopic, 0, [data]);
			ln(" ---> Relayed message to kafka: " + data)
		}
	});

	conn.on('error', function(err) { ln("websocketserver is throwing an error"); });

	// Connect to kafka and handle messages from the chatroom topic
	var consumer = new kafka_native.Consumer({
	    broker: broker,
	    topic: kafkatopic,
	    offset_directory: './kafka-offsets',
	    receive_callback: function(data) {
	        data.messages.forEach(function(message) {
	            console.log('message: ', message.topic, message.partition, message.offset, message.payload);
				ln("Got message from kafka:    "+ message);
				conn.sendUTF(message.payload.replace(/\\/g, ''));
				ln(" ---> Relayed message to client: "+ message.payload);
	        });
	        return Promise.resolve();
	    }
	});
	consumer.start();

	// Handle client disconnections
	conn.on('close', function(conn) { 
		ln(conn.remoteAddress +  " disconnected") 
		ln("closing kafka consumer");
		consumer.stop()
		ln("closing kafka producer");
		producer.stop();
	} );
});
